# codecv

这是一款使用 `markdown` 制作简历的工具，它可以将你编写的`markdown` 简历转换为 `PDF`，支持多种模板，完全免费，用爱发电。

<div style="font-size: 1.5rem;">
  <a href="./README.md">中文</a> |
  <a href="./README.en.md">English</a>
</div>
</br>

[在线编辑地址 1](http://codeleilei.gitee.io/markdown2pdf/) [在线编辑地址 2](https://acmenlei.github.io/codecv/dist/)

> 声明：此项目发布于 GitHub/Gitee，基于 MIT 协议，免费且作为开源学习使用，使用业余时间进行持续开发，部署请注明原仓库地址，尊重作者成果。

## 🤩 效果预览

<p>简历模板和默认主题</p>

<img style="max-width: 1000px" src="./docs/templates.webp" alt="模板" />

<p>简历编辑和暗黑主题</p>

<img style="max-width: 1000px" src="./docs/editor.webp" alt="编辑页" />

<p>内置多种矢量图标</p>

<img style="max-width: 1000px" src="./docs/iconfont.webp" alt="矢量图标" />

## ✊🏻 待实现功能

[✓] 移动端适配

[✓] 内容模式体验优化

[✓] 模板设计（持续更新... 欢迎为仓库贡献模板）

## 🤔 常见问题

[语法问题请查看使用指南](https://codeleilei.gitee.io/markdown2pdf/#/syntax/helper)

**Q**: 为什么导出 `PDF` 后乱码？

**A**: 可能是缓存了旧的字体，请点击预览顶部工具栏中的重置简历内容进行重置，当然重置前请保证内容你已经保存

**Q**: 为什么导出失败？

**A**: 目前服务部署在 `netlify serverless` 服务上，因为是国外服务器，访问容易出错，请多尝试几遍，当然你也可以使用本地导出 `PDF` 替换

## 👋 参与贡献

贡献之前请先阅读[贡献指南](./CONTRIBUTING.md)

## 🙏 赞助

如果你觉得这个项目对你有帮助，并且情况允许的话，可以给我一点点支持，总之非常感谢支持～

<div style="display: flex; gap: 20px;">
	<div style="text-align: center">
		<p>WeChat</p>
		<img style="width: 165px" src="./docs/wechat.jpg" alt="微信" />
	</div>
	<div style="text-align: center">
		<p>Alipay</p>
		<img style="width: 150px" src="./docs/alipay.jpg" alt="支付宝" />
	</div>
</div>

## License

MIT © [Coderlei](./license)
