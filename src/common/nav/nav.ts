const nav = [
  {
    name: '导入/导出',
    multiple: true,
    children: ['导入MD', '导出MD']
  },
  {
    name: '简历模板',
    path: '/template',
    tooltip: false
  },

  {
    name: '语法助手',
    path: '/syntax/helper',
    tooltip: false
  },
  {
    name: '岗位推荐',
    path: '/recruit',
    tooltip: false
  }
]

export default nav
