const content = `::: headStart
# 小C - Web前端开发工程师
icon:user 男 / 2000.01.01 &nbsp;/ &nbsp;icon:phone 155xxxxxx06 &nbsp; /&nbsp; icon:email xxxxxxxx@163.com
icon:github [github.com/acmenlei](https://github.com/acmenlei)&nbsp;icon:gitee [gitee.com/codeleilei](https://gitee.com/codeleilei)&nbsp;icon:juejin [juejin.cn/user/2586468969632445](https://juejin.cn/user/2586468969632445)
::: headEnd

## 教育背景
::: start
**江西xxxx大学**
:::
信息管理学院 - 计算机科学与技术 - GPA 4.0
:::
**2019.09 - 2023.07**
::: end

## 专业技能
- **基础**：熟悉 JavaScript、ES6+ 语法，如 Promise、Async/Await 等，对常见的设计模式和数据结构与算法，如 链表、树、图论等都比较熟悉，并能根据实际应用场景对功能做优化
- **框架**：熟悉 Vue.js、React.js 并对其源码有过阅读，深入理解 Vue.js 相关技术栈，并能在项目中进行实际应用
- **网络**：熟悉计算机网络相关知识，如 TCP/IP、UDP、HTTP、HTTPS、DNS 协议等
- **工程化**：熟悉前端工程化，能使用 Vite、Webpack 对项目进行配置，并具有一定的构建优化实践
- **服务端**：了解 Node.js 及其生态，如 Express，Nest，并有过相关项目实践
- **数据库**：了解 MySQL、Mongodb、Redis 的基本使用，并结合 Sequelize 有过相关项目实践

## 实习经历
::: start
**icon:alibaba 阿里巴巴 - 飞猪 - Web前端开发实习生**
:::
**2022.06 - 2022.09**
::: end

- **技术栈**：\`Rax\` \`Node\` \`Serverless\`
- **工作内容**：xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

::: start
**icon:bytedance  字节跳动 - 飞书 - Web前端开发实习生**
:::
**2022.02 - 2022.06**
::: end

- **技术栈**：\`React\` \`Antv\` \`Node\`
- **工作内容**：xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

## 开源作品
::: start
#### icon:github  codecv&nbsp;&nbsp;icon:star 430+
:::
**2022.09 - 至今**
::: end
- **工具介绍**：这是一款可高度扩展的 markdown 简历制作工具，它可以将你编写的 markdown 内容转换为 PDF，支持多种模板，内置两种编辑模式，Markdown 模式和内容模式数据同步无缝切换
- **代码仓库**：[https://github.com/acmenlei/codecv](https://github.com/acmenlei/codecv)
- **线上演示**：[https://codeleilei.gitee.io/markdown2pdf](https://codeleilei.gitee.io/markdown2pdf)
::: start
#### icon:github  markdown-plus&nbsp;&nbsp;icon:star 6
:::
**2022.09 - 至今**
::: end
- **工具介绍**：弥补 Markdown 本身的缺陷，在一些常用语法的基础上扩展了多列布局、图标等语法
- **代码仓库**：[https://github.com/acmenlei/markdown-plus](https://github.com/acmenlei/markdown-plus)
- **线上演示**：[https://acmenlei.github.io/markdown-transform-html-demo/dist/](https://acmenlei.github.io/markdown-transform-html-demo/dist/)

## 自我评价
自驱力强，对大前端有浓厚的兴趣，持续关注前端领域新玩具/新特性并投入项目使用，保持持续学习的心态 ...`

export default {
  name: '校招/社招/互联网/简洁',
  font: 'Noto Sans SC',
  primaryColor: '#140505',
  primaryBackground: '#140B25',
  img: 'https://z4a.net/images/2023/07/28/campus_simple.png',
  content
}
